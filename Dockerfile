#this is the version of node we will be using . Might update to the latest one but this will work.
FROM node:10

# Creating the working directory we will be using for the application. 
WORKDIR /src/app

COPY package*.json ./
# RUN ls 
RUN npm install
RUN npm install -g typescript
# Bundle app source
COPY . .

RUN tsc --build tsconfig.json


EXPOSE 8071

CMD [ "npm", "start" ]

