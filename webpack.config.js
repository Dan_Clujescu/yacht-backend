module.exports = {
    mode: "development",
    entry: "./src/index.ts",
    resolve: {
        extensions: [".ts"]
    },
    module: {
        rules: [
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            {
                test: /\.tsx?$/,
                exclude: /node-modules/,
                loader: "ts-loader",
                options: {
                    compilerOptions: {
                        outDir: './dist'
                    }
                }
            }
        ]
    }
};
